# Title: test.sim.2
# Author: Derek Neil
# Purpose: Test iret command directly
# How test works:
#	run .xas file directly
#   ./xsim 10000 test.sim.2.xo 1000 > test.sim.2.out
#	load a stack to push and pop form
#	sets r0 to 10110 (0x16)
#	push r0 to a stack twice, as both the iret state and pc
#	use std to activate cpu print outs
#	
#	
# Expected result: 
#	first cpu output should have a state of 2 and r00:0016 and
#		the next instruction to run should be iret
#	the second output should be the result of the iret command
#		the PC should read 0016
#		the state should read 0016
#		the r00 should still read 0016 and
#		the next instruction should be loadi 2, r0
#	the third output should show r00:0002
#		the next instruction should be null
#	the output should match test.sim.2.out exactly
#